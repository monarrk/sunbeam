BOARD = arduino:avr:uno

SYNTH_SERIAL ?= 1

PORT ?= $$( arduino-cli board list | grep ${BOARD} | grep -o "^\S*" )

# define the wave constants
CFLAGS = --build-property "build.extra_flags=\"-O3\""

all: synth

target-install:
	arduino-cli core install arduino:avr

synth: sunbeam.ino
	arduino-cli compile -b ${BOARD} ${CFLAGS} sunbeam.ino

upload: synth
	arduino-cli upload -p ${PORT} -b ${BOARD} sunbeam.ino

monitor: upload
	screen ${PORT}

fmt:
	clang-format -i sunbeam.ino

port:
	@echo ${PORT}
