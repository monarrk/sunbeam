#include <MozziGuts.h>
#include <Oscil.h>
#include <tables/sin2048_int8.h>

// use: Oscil <table_size, update_rate> oscilName (wavetable), look in .h file of table #included above
Oscil<SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);

// use #define for CONTROL_RATE, not a constant
// Hz, powers of 2 are most reliable
#define CONTROL_RATE 64

int pot0, pot1;
int volume, frequency;

void setup() {
	startMozzi(CONTROL_RATE);
}

void updateControl() {
	pot0 = mozziAnalogRead(A0);
	pot1 = mozziAnalogRead(A1);
	frequency = pot0 + 50;
	volume = map(pot1, 0, 1023, 0, 255);
	aSin.setFreq(frequency);
}

AudioOutput_t updateAudio() {
	return MonoOutput::from8Bit((aSin.next() * volume) >> 8);
}

void loop() {
	audioHook();
}
